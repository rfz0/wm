#!/bin/sh

cpu()
{
	awk '{
		if ($1 == "cpu") {
			a[NR,1] = $2
			a[NR,2] = $3
			a[NR,3] = $4
			a[NR,4] = $5
			if (FNR == NR) {
				system("sleep 1")
			}
			nextfile
		}
	}
	END {
		a[2,1] -= a[1,1]
		a[2,1] += a[2,2] - a[1,2]
		a[2,1] += a[2,3] - a[1,3]
		a[2,4] -= a[1,4]
		printf "CPU:%6.2f%%\n", (a[2,1] / (a[2,1] + a[2,4])) * 100
	}' /proc/stat /proc/stat
}

fs_get()
{
	df $1 | awk 'NR == 2 { printf " (%s) %s\n", $6, $5; exit }'
}

fs()
{
	local t=`mktemp`
	for i in $@; do fs_get $i >> $t & done
	wait
	local s=`sort $t | tr -d '\n'`
	rm $t
	[ "$s" ] && echo "DU:$s"
}

disk_get()
{
	awk -v x=$1 'BEGIN { i = 1 }
	{
		if ($3 == x) {
			a[i,1] = $6
			a[i,2] = $10
			if (FNR == NR) {
				system("sleep 1")
			}
			i++
			nextfile
		}
	}
	END {
		a[2,1] = (a[2,1] - a[1,1]) * 512
		a[2,2] = (a[2,2] - a[1,2]) * 512

		for (i=1; i<=2; i++) {
			if (a[2,i] > 1073741824) {
				a[2,i] /= 1073741824.0
				s[i] = "gb"
			} else if (a[2,i] > 1048576) {
				a[2,i] /= 1048576.0
				s[i] = "mb"
			} else if (a[2,i] > 1024) {
				a[2,i] /= 1024.0
				s[i] = "kb"
			} else {
				s[i] = "b"
			}
		}

		printf " (%s) rd:%6.2f %2s/s wr:%6.2f %2s/s\n", x,
			a[2,1], s[1], a[2,2], s[2]
	}' /proc/diskstats /proc/diskstats
}

disk()
{
	local t=`mktemp`
	for i in $@; do disk_get $i >> $t & done
	wait
	local s=`sort $t | tr -d '\n'`
	rm $t
	[ "$s" ] && echo "DISK:$s"
}

load()
{
	awk '{print "LOAD:", $1, $2, $3; exit}' /proc/loadavg
}

mem()
{
	awk '/^MemTotal/{a=$2; next}
		/^MemFree/{b+=$2; next}
		/^Buffers/{b+=$2; next}
		/^Cached/ {b+=$2; exit}
		END {printf "MEM:%6.2f%%\n", ((a-b)/a)*100}' \
			/proc/meminfo
}

net_get()
{
	awk -v x=$1 '{
		if (NR == 1) {
			a[1] = $0
			nextfile
		} else if (NR == 2) {
			a[2] = $0
			system("sleep 1")
			nextfile
		} else if (NR == 3) {
			a[1] = $0 - a[1]
			nextfile
		} else if (NR == 4) {
			a[2] = $0 - a[2]
			nextfile
		}
	}
	END {
		for (i=1; i<=2; i++) {
			if (a[i] > 1073741824) {
				a[i] /= 1073741824.0
				s[i] = "gb"
			} else if (a[i] > 1048576) {
				a[i] /= 1048576.0
				s[i] = "mb"
			} else if (a[i] > 1024) {
				a[i] /= 1024.0
				s[i] = "kb"
			} else {
				s[i] = "b"
			}
		}
		printf " (%s) tx:%6.2f %2s/s rx:%6.2f %2s/s\n", x,
		       a[1], s[1], a[2], s[2]
	}' /sys/class/net/$1/statistics/tx_bytes \
		/sys/class/net/$1/statistics/rx_bytes \
		/sys/class/net/$1/statistics/tx_bytes \
		/sys/class/net/$1/statistics/rx_bytes
}

net()
{
	local t=`mktemp`
	for i in $@; do net_get $i >> $t & done
	wait
	local s=`sort $t | tr -d '\n'`
	rm $t
	[ "$s" ] && echo "NET:$s"
}

swap()
{
	awk '/^SwapTotal/{a=$2; next}
		/^SwapFree/{b=$2; exit}
		END{printf "SWAP:%6.2f%%\n", ((a-b)/a)*100}' \
			/proc/meminfo
}

net_names()
{
	ip link show |
		awk -v x="$1" -F: 'BEGIN {n = split(x, a, " ")}
		{for (i=1; i<=n; i++)
			if ($1 ~ /^[0-9]/ && $2 ~ a[i] && $3 ~ /UP/)
				{s=s" "$2; next}
		} END {print s}'
}

fs_names()
{
	awk -v x="$1" 'BEGIN {n = split(x, a, " ")}
	{for (i=1; i<=n; i++) if ($3 == a[i]) {s=s" "$2; next}}
	END {print s}' /proc/mounts
}

disk_names()
{
	awk -v x="$1" 'BEGIN {n = split(x, a, " ")}
	{for (i=1; i<=n; i++)
		if ($4 ~ a[i] && $4 !~ /[0-9]/)
			{s=s" "$4; next}
	} END {print s}' /proc/partitions
}

fs_types="ext4 vfat"
disk_types="sd"
net_types="enp"

t=`mktemp`
(cpu >> $t) &
(fs `fs_names "$fs_types"` >> $t) &
(disk `disk_names "$disk_types"` >> $t) &
(load >> $t) &
(mem >> $t) &
(net `net_names "$net_types"` >> $t) &
(swap >> $t) &
wait
load=`grep '^LOAD' $t`
cpu=`grep '^CPU' $t`
mem=`grep '^MEM' $t`
swap=`grep '^SWAP' $t`
fs=`grep '^DU' $t`
disk=`grep '^DISK' $t`
net=`grep '^NET' $t`
sort $t | paste -d '|' -s -
rm $t
